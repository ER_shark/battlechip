var Game = require('./models/Game.js')
var User = require('./models/User.js')


// A variável socket será socket quando 1 determinado utilizador pedir a lista de jogos (ao entrar na lista de jogos) -> é atualizado apenas nesse que pediu e não a todos
// A variável socket será io quando 1 novo jogo for criado (logo a lista de jogos é atualizada em todos e não em apenas 1)
function gamesList(socket) {       

    // Procura todos os jogos na tabela Game e devolve na variável games
    Game.find({}).then(function(games) {
        var gameList = [];

        // Percorre todos os jogos
        games.forEach(function(game) {
            if (game.players.length < 2) { // Devolve o jogo apenas (adiciona ao array) se não estiver lotado
                gameList.push({id: game._id, name: game.name, users: game.players.length, max: 2});
            }
        });

        // Envia para os jogadores a lista de jogos (para atualizar essa mesma lista)
        socket.emit("placeGames", gameList);

        return gameList;

    });
}





// Esta função devolverá os jogos não finalizados de um jogador
function notConcludedGamesOfUser (userID, socket) {

    var listOfGames = [];

    // Procura os jogos não finalizados de um jogador
    Game.find({}).then(function(games) {
        
        // Percorre todos os jogos
        games.forEach(function(game) {

            if (game.players.includes(userID) && !game.finished) {
                listOfGames.push({id: game._id, name: game.name, users: game.players.length, max: 2});
            }

        });


        // Envia para os jogadores a lista de jogos (para atualizar essa mesma lista)
        socket.emit("placeNotConcludedGames", listOfGames);

        return listOfGames;

    });    


}






// Esta função devolverá a lista de rankings
function rankingsList (socket) {

    var usersList = []; // Guarda a lista de utilizadores
    var rankingsList = []; // Guarda os rankings
    var gamesWon = 0, gamesLost = 0; // Guarda o nº de jogos ganhos e perdidos de cada jogador

    var position = 0;


    User.find({}).then(function (users) {

        users.forEach(function (user) { // Vai percorrer todos os utilizadores para ir buscar as suas vitórias e derrotas

            Game.find({}).then(function(games) {
                
                // Percorre todos os jogos
                games.forEach(function(game) {
                    
                    if (game.players.includes(user._id) && game.finished) {

                        position = game.players.indexOf(user._id); //  Vai buscar a posição do array de jogadores deste utilizador

                        if (game.winner == position) { // Se a posição do array guardada no winner for a mesma, quer dizer que foi o vencedor
                            gamesWon++;
                        } else {
                            gamesLost++;
                        }

                    }
        
                });

                // Adiciona o utilizador com as suas derrotas e vitórias
                rankingsList.push({id: user._id, name: user.username, gamesWon: gamesWon, gamesLost: gamesLost});

                if (rankingsList.length == users.length) { // Só envia depois de ter todos

                    // Envia para os jogadores a lista de rankings
                    socket.emit("rankingsList", rankingsList);

                }

                gamesWon = 0;
                gamesLost = 0;

        
            });   
    
    

        });


        
    });

    return rankingsList;

}





// Esta função avisa aos dois jogadores quem é que começa o jogo
function tellWhoStarts (gameID, whoStarts, io) {

    console.log("WHO STARTS" + whoStarts);

    // Servidor avisa aos 2 jogadores qual é que joga no momento
    io.sockets.in('room' + gameID).emit("currentPlayer", whoStarts);

    io.sockets.in('room' + gameID).emit("startPlaying", gameID); // Servidor avisa aos 2 jogadores que o jogo começará (pois já foi escolhido quem começa)


    // Envia mensagem para o chat a indicar quem começa o jogo
    io.sockets.in('room' + gameID).emit("receivedMessage", "O jogador " + io.sockets.adapter.rooms["room" + gameID].playersNames[whoStarts] + " começa o jogo.");


}




// Função para entrar num determinado jogo
function joinGame(gameID, io, socket, userID, userName) {

    var playersList = [];


    // Procura os dados de um jogo pelo id do jogo, e devolve na variável game
    Game.findById(gameID, '_id name players', function (err, game) {

        // Guarda uma cópia da lista de jogadores
        playersList = game.players.slice();

        if (game.players.length < 2) { // Só pode entrar no jogo se tiver menos de 2 pessoas

            playersList.push(userID); // Adicionar o jogador que quer entrar à lista de jogadores do quarto

            // Atualizar a lista de jogadores desse jogo (adiciona a pessoa que entrou)
            Game.findByIdAndUpdate(gameID, { players: playersList }, function(err, result) {
                
                // Cada vez que um utilizador entra num jogo, tem de ser atualizado na lista de jogos de todos os utilizadores
                gamesList(io); // Chama a função para enviar a lista de jogos atualizada para todos os jogadores (para o io)

            });

        }


        // Envia mensagem para o chat a indicar que um jogador entrou no jogo.
        // io.sockets.in('room' + gameID).emit("receivedMessage", "O jogador maria entrou no jogo.");
        

        // Envia mensagem para o jogador que entrou no jogo (para ir para o game lobby)
        io.sockets.in('room' + gameID).emit("roomJoined", gameID);



    });

}



// Função em que o servidor recebe informação de que todos os barcos foram colocados, e envia mensagem quando receber a informação que todos os barcos foram colocados
function allBoatsPlaced(gameID, io) { // Quando os dois jogadores estão prontos

    // Esta função é chamada quando os dois jogadores estão prontos (já puseram os barcos todos)
    io.sockets.in('room' + gameID).emit("startRollingDice", gameID); // Servidor avisa aos 2 jogadores para rodarem o dado a ver qual é o primeiro a jogar (pois ambos já colocaram os barcos)

    // Envia mensagem para o chat a indicar que ambos estão prontos
    io.sockets.in('room' + gameID).emit("receivedMessage", "Ambos os jogadores estão prontos.");

}





// Esta função remove todos os barcos do utilizador num determinado jogo
function resetAllBoats(gameID, numPlayer, io) {

    // Envia mensagem para o chat a indicar que um jogador apagou todos os barcos.
    io.sockets.in('room' + gameID).emit("receivedMessage", "O jogador " + io.sockets.adapter.rooms["room" + gameID].playersNames[numPlayer] + " removeu todos os barcos.");

    var gameMatrix = [];
    var boatsNumbers = ["A", "B", "C", "D", "E"]; // As letras referentes aos barcos (que os identificam na matriz)


    Game.findById(gameID, '_id boards', function (err, game) {

        // Vai buscar a matriz guardada à base de dados
        gameMatrix = game.boards.slice();


        var cell_id = "";


        // Percorre a matriz completa do utilizador numPlayer
        for (var i=0; i<10; i++) {
            for (var j=0; j<10; j++) {
                
                if (boatsNumbers.includes(gameMatrix[numPlayer][i][j])) { // Todas as células que tiverem letras referentes a barcos
                    
                    gameMatrix[numPlayer][i][j] = 'X'; // Vai substituir todas essas células por 'X' (não tem barco nenhum - está livre)                              
                    
                    cell_id = String.fromCharCode(65+j) + "" + (i+1); // Converter (0,0) para A1, (0,1) para A2, etc
                           
                    // Envia para o jogador que fez reset (ele próprio), para atualizar o quadro
                    var data = [cell_id, numPlayer, 'X'];
                    io.sockets.in('room' + gameID).emit("changeCell", data);
                    

                }

            }
        }

        // Atualiza a matriz na base de dados, referente ao jogo com o gameID
        Game.findByIdAndUpdate(gameID, { boards: gameMatrix }, function(err, result) {
                
            
        });


    });

    
}



// Esta função vai guardar na matriz do jogo a posição onde foi colocado o barco
function savePlacedBoat(cell_id, gameID, numPlayer, boatNum, io, randomBoat) {


    var matrix = [];
    var boat = [];

    var boatsNumbers = ["A", "B", "C", "D", "E"]; // As letras referentes aos barcos (que os identificam na matriz)



    var countChanges = 0; // Conta o nº de células que apagou (para verificar se foi necessário apagar ou não os barcos existentes)


    Game.findById(gameID, '_id boards', function (err, game) {

        // Vai buscar a matriz guardada à base de dados
        matrix = game.boards.slice();


        if (randomBoat) {

            var cellID = "";
                
            // LIMPAR A MATRIZ TODA ANTES DE COLOCAR NOVOS BARCOS RANDOM
            // Percorre a matriz completa do utilizador numPlayer para limpar toda, antes de colocar os barcos automáticos (random)
            for (var m=0; m<10; m++) {
                for (var n=0; n<10; n++) {
                    
                    if (boatsNumbers.includes(matrix[numPlayer][m][n])) { // Todas as células que tiverem letras referentes a barcos
                        
                        matrix[numPlayer][m][n] = 'X'; // Vai substituir todas essas células por 'X' (não tem barco nenhum - está livre)                              
                        
                        countChanges++; // Foi apagada uma célula do barco

                        cellID = String.fromCharCode(65+n) + "" + (m+1); // Converter (0,0) para A1, (0,1) para A2, etc
                            
                        // Envia para o jogador que fez reset (ele próprio), para atualizar o quadro
                        var data = [cellID, numPlayer, 'X'];
                        io.sockets.in('room' + gameID).emit("changeCell", data);

                    }

                }
            }


            // Caso tenham sido apagadas células de barcos
            if (countChanges > 0) {
                        
                // Envia mensagem para o chat a indicar que um jogador apagou todos os barcos.
                io.sockets.in('room' + gameID).emit("receivedMessage", "O jogador " + io.sockets.adapter.rooms["room" + gameID].playersNames[numPlayer] + " removeu todos os barcos.");

            }



        }



        if (randomBoat) {

            // Envia mensagem para o chat a indicar que um jogador colocou 5 barcos.
            io.sockets.in('room' + gameID).emit("receivedMessage", "O jogador " + io.sockets.adapter.rooms["room" + gameID].playersNames[numPlayer] + " colocou 5 barcos.");
        
        } else {
    
            // Envia mensagem para o chat a indicar que um jogador colocou 1 barco.
            io.sockets.in('room' + gameID).emit("receivedMessage", "O jogador " + io.sockets.adapter.rooms["room" + gameID].playersNames[numPlayer] + " colocou 1 barco.");
        
        }
        
        

        // Percorre todas as células em que foi colocado o barco (randomBoat = false) ou todas as células de todos os barcos (randomBoat = true)
        for (var i=0; i<cell_id.length; i++) {

            if (randomBoat) { // Se forem barcos random/automáticos, são colocados todos de uma vez  (quer) (cell_id representa array de barcos, ou seja, array de array de posições)


                boat = cell_id[i];

                for (var y=0; y<cell_id[i].length; y++) { // Percorrer cada um dos barcos pois são enviados todos de uma vez
                    
                    // Vai buscar a linha e coluna de cada barco
                    var rowIndex = boat[y].substr(1, boat[y].length); // 2º caratere (começa no index 1 e tem tamanho de 1)
                    var columnIndex = parseInt(boat[y].substr(0,1).charCodeAt(0)) - 65; // Converter caratere para número
                    // Coloca o nº do barco na matriz, nas células onde foi adicionado o barco
                    matrix[numPlayer][rowIndex-1][columnIndex] = boatNum[i];

                }

            } else { // Se não forem barcos random, são colocados 1 de cada vez (cell_id representa array de posições)

                // Vai buscar a linha e coluna de cada barco
                var rowIndex = cell_id[i].substr(1, cell_id[i].length); // 2º caratere (começa no index 1 e tem tamanho de 1)
                var columnIndex = parseInt(cell_id[i].substr(0,1).charCodeAt(0)) - 65; // Converter caratere para número
                // Coloca o nº do barco na matriz, nas células onde foi adicionado o barco
                matrix[numPlayer][rowIndex-1][columnIndex] = boatNum;

            }

        }


        // Atualiza a matriz na base de dados, referente ao jogo com o gameID
        Game.findByIdAndUpdate(gameID, { boards: matrix }, async function(err, result) {
            

        });


    });

    

}



// Esta função avisa quando um barco for totalmente destruído
function oponentBoatExploded(gameID, numPlayer, io, boatType, boatPos, count) {
    
    var oponent = 0;

    // Vai buscar as matrizes do jogo com este id gameID
    Game.findById(gameID, '_id boards', function (err, game) {

        // Vai buscar a matriz guardada à base de dados (faz cópia)
        matrix = game.boards.slice();

        // Este método é chamado quando o jogador dispara e acerta num barco do adversário
        // Logo, é necessário verificar a matriz do outro jogador (adversário), a ver se está completa (todos os barcos atingidos)
        if (numPlayer == 0) oponent = 1; // Se o jogador 0 não tem mais barcos por atingir, o vencedor é o 1
        if (numPlayer == 1) oponent = 0; // Se o jogador 1 não tem mais barcos por atingir, o vencedor é o 0

    

        // Se o barco já explodiu (não tem mais células dele para atingir) é enviada mensagem para a sala
        if (count == 0) { // A contagem é feita após atualizar a matriz, logo deve ser 0 e não 1
            
            var boatExploded = [numPlayer, boatType, boatPos];
            io.sockets.in('room' + gameID).emit("boatExploded", boatExploded); // Envia para o quarto qual foi o barco que explodiu e de qual jogador


            // Envia mensagem para o chat a indicar que um jogador colocou um barco.
            io.sockets.in('room' + gameID).emit("receivedMessage", "O barco do jogador " + io.sockets.adapter.rooms["room" + gameID].playersNames[oponent] + " foi destruído.");


            var cell_id = "", rowIndex = "", columnIndex = "";

            // Envia todas as posições do barco para avisar que explodiu o barco todo (para mudar na grid)
            for(var z = 0; z < boatPos.length; z++) {

                cell_id = boatPos[z];

                // Percorre todas as células em que foi colocado o barco
                rowIndex = cell_id.substr(1, cell_id.length-1); // 2º caratere (começa no index 1 e tem tamanho de 1)
                columnIndex = parseInt(cell_id.substr(0,1).charCodeAt(0)) - 65; // Converter caratere para número
                
                // Coloca um '0' na matriz, nas células onde o barco explodiu, para indicar que explodiu
                matrix[oponent][rowIndex-1][columnIndex] = 'O';

                // Envia para o jogador do quarto que foi atingido (o adversário), para atualizar o quadro
                var data = [cell_id, oponent, 'O'];
                io.sockets.in('room' + gameID).emit("changeCell", data);
            } 


            // Atualiza a matriz na base de dados, referente ao jogo com o gameID
            Game.findByIdAndUpdate(gameID, { boards: matrix }, function(err, result) {
                    
            });
    

        }


    });

}




// Esta função devolve o vencedor de um jogo
function getWinner(gameID, io) {

    var winner = "";

    // Vai buscar as matrizes do jogo com este id gameID
    Game.findById(gameID, '_id players winner', function (err, game) {

        winner = io.sockets.adapter.rooms["room" + gameID].playersNames[game.winner];

        io.sockets.in('room' + gameID).emit("sendWinner", winner);

    });

}





// Esta função devolve se já foram atingidos todos os barcos do adversário
function allOponentBoatsHit(gameID, numPlayer, io) {

    var matrix = [];
    var countBoats = 0;
    var oponent = 0;
    var boatsNumbers = ["A", "B", "C", "D", "E"];

    
    Game.findById(gameID, '_id boards', function (err, game) {

        // Vai buscar a matriz guardada à base de dados (faz cópia)
        matrix = game.boards.slice();
    
                
        // Este método é chamado quando o jogador dispara e acerta num barco do adversário
        // Logo, é necessário verificar a matriz do outro jogador (adversário), a ver se está completa (todos os barcos atingidos)
        if (numPlayer == 0) oponent = 1; // Se o jogador 0 não tem mais barcos por atingir, o vencedor é o 1
        if (numPlayer == 1) oponent = 0; // Se o jogador 1 não tem mais barcos por atingir, o vencedor é o 0


        // Percorre a matriz toda do jogador adversário (representado no oponent) e conta o número de barcos ainda por disparar (o nº de 'B' na matriz)
        for (var i=0; i<10; i++) {    
            for (var j=0; j<10; j++) {

                if (boatsNumbers.includes(matrix[oponent][i][j])) { // Se naquela posição estiver um dos valores dos barcos (A, B, C, D ou E)
                    countBoats++; // Soma o número de barcos em que o adversário ainda tem por disparar 
                }

            }
        }


        // No momento em que faz a verificação, ainda não alterou na base de dados (daí ser == 1 e não == 0)
        // Se o jogador adversário não tiver mais barcos não atingidos, na sua matriz, então o jogador venceu
        if (countBoats == 1) { // Esta função só é chamada quando atinge, logo, quando atinge, tem de ter lá apenas 1 barco por atingir (que é o que está a ser atingido no momento)
            
            io.sockets.in('room' + gameID).emit("winner", numPlayer);


            // Guardar o vencedor e indicar que o jogo terminou
            Game.findByIdAndUpdate(gameID, { finished: true, winner: numPlayer }, function(err, result) {
                
            });

            
            // Envia mensagem para o chat a indicar quem foi o vencedor.
            io.sockets.in('room' + gameID).emit("receivedMessage", "O jogador " + io.sockets.adapter.rooms["room" + gameID].playersNames[numPlayer] + " venceu o jogo.");
            
            
            
            getWinner(gameID,  io);

        }

    });

}



// Esta função é chamada quando é dado um tiro
function shot(cell_id, gameID, numPlayer, io) {
    
    var matrix = [];
    var oponent = 0;
    var nextPlayer = 0; // Próximo jogador que vai jogar (após o shot)

    var boatsNumbers = ["A", "B", "C", "D", "E"]; // Letras que representam cada barco
    var hitBoatsNumbers = ["Q", "R", "S", "T", "U"]; // Letras que representam cada barco após atingido
    
    Game.findById(gameID, '_id boards', function (err, game) {

        // Vai buscar a matriz guardada à base de dados
        matrix = game.boards.slice();


        // Se o jogador 0 der um tiro
        if (numPlayer == 0) { // Tem de colocar o tiro na matriz do jogador 1
            oponent = 1;
        } else { // Se o jogador 1 der um tiro
            oponent = 0; // Tem de colocar o tiro na matriz do jogador 0
        }
        

        // Percorre todas as células em que foi colocado o barco
        var rowIndex = cell_id.substr(1, cell_id.length-1); // 2º caratere (começa no index 1 e tem tamanho de 1)
        var columnIndex = parseInt(cell_id.substr(0,1).charCodeAt(0)) - 65; // Converter caratere para número
        // Coloca um 'B' na matriz, nas células onde foi adicionado o barco

        if (matrix[oponent][rowIndex-1][columnIndex] == 'X') { // Se não tem nada nessa posição atingida

            matrix[oponent][rowIndex-1][columnIndex] = 'W'; // Coloca lá WATER

            nextPlayer = oponent; // Se não acertou no barco, o próximo jogador é o adversário

            // Envia mensagem para o chat a indicar que não acertou
            io.sockets.in('room' + gameID).emit("receivedMessage", "O jogador " + io.sockets.adapter.rooms["room" + gameID].playersNames[numPlayer] + " não acertou. Próximo jogador: " + io.sockets.adapter.rooms["room" + gameID].playersNames[nextPlayer]);


        } else if (boatsNumbers.includes(matrix[oponent][rowIndex-1][columnIndex])) { // Se tem um barco nessa posição atingida, vai depender do tipo de barco
            
            var position = boatsNumbers.indexOf(matrix[oponent][rowIndex-1][columnIndex]); // Saber a posição em que se encontra o tipo de barco, para ir buscar a letra que representa esse mesmo barco atingido
            var boatHitVal = hitBoatsNumbers[position];
            var boatLetter = boatsNumbers[position];

            matrix[oponent][rowIndex-1][columnIndex] = boatHitVal; // Coloca lá SHOT (a letra depende do tipo de barco)

            nextPlayer = numPlayer; // Se acertou no barco, o próximo jogador é ele próprio (joga novamente)

            // Envia mensagem para o chat a indicar que não acertou
            io.sockets.in('room' + gameID).emit("receivedMessage", "O jogador " + io.sockets.adapter.rooms["room" + gameID].playersNames[numPlayer] + " acertou. Continua a jogar.");



            // Apenas é verificado se todos os barcos do adversário foram atingidos, quando acertamos (shot)
            // Verificar se o jogador que deu o tiro (numPlayer) já atingiu todos os outros barcos
            allOponentBoatsHit(gameID, numPlayer, io); // A cada jogada feita, é necessário verificar se já chegou ao fim do jogo
            

        }


        // Atualiza a matriz na base de dados, referente ao jogo com o gameID
        Game.findByIdAndUpdate(gameID, { boards: matrix }, function(err, result) {
                
            // console.log("NUNO" + matrix[numPlayer][rowIndex-1][columnIndex]);
            // Envia para o jogador do quarto que foi atingido (o adversário), para atualizar o quadro
            var data = [cell_id, oponent, matrix[oponent][rowIndex-1][columnIndex]];
            io.sockets.in('room' + gameID).emit("changeCell", data);

            if (hitBoatsNumbers.includes(matrix[oponent][rowIndex-1][columnIndex])) { // Apenas verifica se atingiu o barco, caso tenha mudado o valor da matriz para um valor que significa barco atingido

                // A cada jogada feita, é necessário verificar se o barco atingido já explodiu (ou seja, se este hit destruiu o barco todo) 
                // for (var i=0; i<boatsNumbers.length; i++) { // Para cada tipo de barco
                    
                    var boatPos = [];
                    var count = 0;

                    // Percorre a matriz toda do jogador adversário (representado no oponent) e conta o número de barcos ainda por disparar (o nº de 'B' na matriz)
                    for (var y=0; y<10; y++) {    
                        for (var j=0; j<10; j++) {

                            if (matrix[oponent][y][j] == boatLetter) {
                                count++;
                            }

                            if (matrix[oponent][y][j] == boatHitVal) {
                                boatPos.push(String.fromCharCode(65+j) + "" + (y+1)); // Converter de 0 para A, 1 para B, 2 para C, etc, para criar a célula (para ficar de (0,0) para A1, (0,1) para A2, etc)
                            }

                        }
                    }

                // }

        
                // console.log("NUNO" + count + " " + boatLetter + " " + boatPos);
                if (boatPos.length > 0) {
                    oponentBoatExploded(gameID, numPlayer, io, boatLetter, boatPos, count); // boatsNumbers[i] representa o tipo de barco (a letra que representa na base de dados)
                }
            }

            // Servidor avisa aos 2 jogadores qual é o jogador que joga no momento
            // Neste caso, o próximo jogador a jogar deve ser o adversário se o jogador atual não acertar, senão o próximo é ele próprio
            io.sockets.in('room' + gameID).emit("currentPlayer", nextPlayer);

            
        });


    });

}





// Esta função permite carregar um determinado jogo (com os dados das matrizes guardados na base de dados)
function loadGame(gameID, io, userID) {

    var matrix;
    
    Game.findById(gameID, '_id boards players finished', function (err, game) {

        // Vai buscar a matriz guardada à base de dados
        matrix = game.boards.slice();

        // Só carrega o jogo caso o utilizador que queira carregar seja jogador desse jogo
        if (game.players.includes(userID) && !game.finished) {
                        
            // Carrega o jogo no quarto
            io.sockets.in('room' + gameID).emit("loadGame", matrix);

        }

        // var cell_id = "";


        /* for (var i=0; i<2; i++) { // Percorre a matriz dos 2 jogadores porque tem de carregar as 2

            for (var y=0; y<10; y++) {    
                for (var j=0; j<10; j++) {

                    cell_id = String.fromCharCode(65+j) + "" + (y+1); // Converter (0,0) para A1, (0,1) para A2, etc
                           
                    var data = [cell_id, i, matrix[i][y][j]];
                    io.sockets.in('room' + gameID).emit("changeCell", data);

                }
            }

        } */


    });

}




// Função em que o servidor indica aos jogadores que o jogo começou e podem colocar os barcos
function startGame(gameID, io) {

    // Envia para todos do jogo a indicar que o jogo começou
    // Logo que começa o jogo, o primeiro passo é sempre colocar os barcos (v-show = true)
    // Emite gameStarted para as pessoas do quarto (apenas os jogadores do quarto)
    io.sockets.in('room' + gameID).emit("gameStarted", gameID); // Servidor avisa aos 2 jogadores que o jogo começará


    // Não é preciso o código abaixo porque no início ele começa sempre com a variável a true
    // Envia para todos do jogo a indicar que podem começar a colocar os barcos
    // io.sockets.in('room' + gameID).emit("startPlacingBoats", gameID); // Servidor avisa aos 2 jogadores que podem colocar os barcos
    
    // Quando o jogo começa, os jogadores que terminaram de colocar os barcos é 0 (porque ainda nenhum acabou de colocar os barcos)
    io.sockets.adapter.rooms["room" + gameID].playersFinished = 0;

    // Quando o jogo começa, nenhum ainda jogou o dado para saber qual o primeiro a começar
    io.sockets.adapter.rooms["room" + gameID].playersFinishedRoll = 0;
    io.sockets.adapter.rooms["room" + gameID].player0 = 0;
    io.sockets.adapter.rooms["room" + gameID].player1 = 0;


}





// Devolve os jogadores do jogo para quem pediu
function getPlayersOfGame(gameID, socket, io) {

    var players = io.sockets.adapter.rooms["room" + gameID].playersNames;

    Game.findById(gameID, 'players name', function (err, game) {
        socket.emit("playersOfGame", {"players": players, "roomname": game.name}); // Devolve os jogadores do jogo
        return game.players;
    });

}



// Função para criar um novo jogo quando é clicado no botão +
function createNewGame(io,roomName){


    // Cria a matriz para cada um dos 2 jogadores
    var boards = [];
    for (var n_board=0; n_board<2; n_board++) {

        boards[n_board] = [];

        for (var i=0; i<10; i++) {

            boards[n_board][i] = [];

            for (var j=0; j<10; j++) {
                boards[n_board][i][j] = 'X';
            }

        }
    }



    var newGame = new Game();
    newGame.players = [];
    newGame.boards = boards; // Atribui a matriz criada
    newGame.name = roomName;
    newGame.winner = 0;
    newGame.date_start = new Date("2019-12-12");
    newGame.date_finish = new Date("2019-12-11");
    newGame.finished = 0;
    newGame
    newGame.save() 
            .then(function(game,_id){ // Se guardar com sucesso
                                
                gamesList(io); // Chama a função para enviar a lista de jogos atualizada para todos os jogadores (para o io)

                
            })
            .catch(function(err){
                console.log("An error occured: "+err);
            })

}


module.exports = {
    createNewGame,
    gamesList,
    notConcludedGamesOfUser,
    joinGame,
    getPlayersOfGame,
    startGame,
    allOponentBoatsHit,
    savePlacedBoat,
    resetAllBoats,
    allBoatsPlaced,
    shot,
    tellWhoStarts,
    oponentBoatExploded,
    loadGame,
    rankingsList,
    getWinner,
}
