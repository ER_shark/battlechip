"use strict";
var mongoose = require("mongoose");
var bcrypt = require("bcryptjs");

const  SALT_WORK_FACTOR = 4; //number of iterations of encryption it will use, the more the most secure, but more computationally intensive
var Schema = mongoose.Schema, ObjectId = Schema.ObjectId;

var UserSchema = new Schema({
    username: {type: String, required: true},
    email: {type: String, required: true, index: true},
    password: {type: String, required: true},
});
//METHODS
/**
 * comparePassword for comparing 
 */
UserSchema.methods.comparePassword = async function(newPassword) {
    return await bcrypt.compare(newPassword, this.password)
    
};
//STATIC METHODS

/**
 * getAccount returns a user if the password matches the said user
 */
UserSchema.statics.getAccount = async function (aEmail, aPassword){

    var user;
    return await mongoose.model('User',UserSchema).findOne({email: aEmail}).exec()
    .then(function(aUser){
        if(aUser == null) {
            return false;
        }else{
            user = aUser;
            return user.comparePassword(aPassword).then(function(res){
                return res
            }).catch(function(err){
                console.log("Passwords dont match "+err)
            })
        }

        
    }).catch(function(err){
        console.log("An error occured fetching user "+err);
    })    
}



/**
 * Devolve o id do utilizador com um determinado email
 */
UserSchema.statics.getUserID = async function (aEmail){

    var user;
    await mongoose.model('User',UserSchema).findOne({email: aEmail}).exec()
    .then(function(aUser){
        user = aUser;
        return user._id;
    }).catch(function(err){
        console.log("An error occured fetching user with email "+err);
        return false;
    })    
}




/**
 * userExists returns true if the user exists
 */
UserSchema.statics.userExists = async function (aEmail){

    return await model('User',UserSchema).countDocuments({email: aEmail})
    .then(function(count){
        if(count > 0) return true;
        return false;
    }).catch(function(err){
        console.log("An error occured checking if user exists")
    })
}

//MIDDLEWARES
/**
 * Adds a middleware that happens before calling the save method(pre)
 * 
 */
UserSchema.pre('save', function(next) {
    var user = this;

    // only hash the password if it has been modified (or is new)
    if (!user.isModified('password')) return next();

    // generate a salt
    bcrypt.genSalt(SALT_WORK_FACTOR, function(err, salt) {
        if (err) return next(err);

        // hash the password using the salt
        bcrypt.hash(user.password, salt, function(err, hash) {
            if (err) return next(err);

            // override the unencrypted password with the hashed one
            user.password = hash;
            next();
        });
    });
    
});


module.exports = mongoose.model('User', UserSchema)