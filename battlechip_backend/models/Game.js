var mongoose = require('mongoose');

var Schema = mongoose.Schema, ObjectId = Schema.ObjectId;


var GameSchema = new Schema({
    players: Array,
    boards: Array,
    name: String,
    date_start: Date,
    date_finish: Date,
    winner: Number,
    finished: Boolean
});

module.exports = mongoose.model('Game', GameSchema);