"use strict";
var express = require('express');
var http = require('http');
var mongoose = require('mongoose');
var session = require('express-session');
var bodyParser = require('body-parser');
var app = express();
var path = require('path');
var socketio = require('socket.io');
var server = http.Server(app); // Servidor da aplicação
var game = require('./game.js');
var auth = require('./auth.js');
const encrypt = require('socket.io-encrypt');
//var history = require('connect-history-api-fallback');

/**
 * Models
 */
mongoose.connect(process.env.MONGO_CONN || 'mongodb://localhost/test', {useNewUrlParser: true,useUnifiedTopology: true});
mongoose.set('useCreateIndex', true); //for supressing warnings
//mongoose.set('useFindAndModify', false);

app.set('view engine', 'ejs');

var sessionsMiddleware = session({
        secret: 'secret',
        resave: true,
        saveUninitialized: true
    })
//for sessions
app.use(sessionsMiddleware);
//for http posts
app.use(bodyParser.urlencoded({extended : true}));
app.use(bodyParser.json());
//app.use(history());
//for allowing cross origin  policy


var io = socketio(server); // Atribuir servidor da aplicação

/**
 * For using sessions in socketio
 */
io.use(function(socket,next){
    sessionsMiddleware(socket.request, socket.request.res,next);
});
io.origins('*:*');
//io.use(encrypt('secret'));

app.set('views', __dirname + '/views/');
app.use('/vue',express.static('static'));
app.use('/vue',express.static(path.resolve(__dirname + '/views/vue/')));

app.use(express.static('public'));
//#region login stuff

app.use(function(req,res,next){
    if(req.originalUrl.includes('register') || req.originalUrl.includes('login') || req.session.isLogged ){
        return next()
    }
    res.redirect('/login',)
})

app.get('/', function(req,res){
    // Se o utilizador não estiver logado, vai para a rota do login
    if(!req.session.isLogged)
        res.redirect('/login');
    else{ // Se o utilizador já estiver logado, vai para a rota do jogo
        res.redirect('/game');
    }

});


// Rota do jogo
app.get('/game', function(req, res){
    if(req.session.isLogged) // Se o utilizador já estiver logado, mostra a rota do game (vue)
        res.render('vue/game');
    else // Se o utilizador não estiver logado, vai para a rota inicial "/"" (que manda para a rota login ou game)
        res.redirect('/');
});

// Rota para fazer logout
app.get('/logout',function(req,res){
    req.session.destroy();
    req.session = null;
    res.redirect('/') // Redirecona para o início

})

// Rota de login
app.get('/login',function(req,res){
    if(req.session.isLogged) res.redirect('/'); // Se o utilizador já está logado, não faz login, vai para o jogo
    
    // Variáveis que envia para o vue
    var route = "login"; // Qual a rota (login ou registo), para incluir no ficheiro index.ejs
    var msg = "";

    if(req.query.route){
        route = req.query.route;
        msg = req.query.msg;
    }

    // Mostra o ficheiro index.ejs e envia essas 2 variáveis para o template (route e msg)
    res.render('index',{data:{route:route, msg:msg}});
});

// Rota do registo
app.get('/register',function(req,res){
    let route = "register"; // Qual a rota (login ou registo), para incluir no ficheiro index.ejs
    let msg = "";

    if(req.query.route){
        route = req.query.route;
        msg = req.query.msg;
    }

    // Mostra o ficheiro index.ejs e envia essas 2 variáveis para o template (route e msg)
    res.render('index',{data:{route:route, msg:msg}});
});

// Na rota login com o método POST, é verificado se os dados do utilizador estão corretos
app.post('/login', auth.login);

// Na rota register com o método POST, é criada uma conta para o utilizador com base nos dados do formulário
app.post('/register', auth.register)

//#endregion



//Se emitirmos em io emite para todos os clientes enquanto se emitirmos em socket  emite só para 1 cliente
var userCount = 0;
io.on('connection',function(socket){ // Ao aceder ao servidor, entra no connection
    userCount ++;
    // io.sockets.clients().length doesnt seem to work
    console.log("Someone connected!"+ socket.id);
    console.log("Sizeof clients " + userCount );



    // Envia o id do utilizador para ele, para guardar no store
    console.log("user id" + socket.request.session.userID);
    socket.emit('sendUserID', socket.request.session.userID);


    // Quando um jogador coloca um barco 
    socket.on('placedBoat', function (sendData){ // este é chamado apenas dentro do jogo
        var cell_id = sendData[0]; // Array com posições (células) do barco
        var gameID = sendData[1]; // Id do jogo
        var numPlayer = sendData[2]; // Número do jogador 
        var boatNum = sendData[3]; // Número do barco
        var randomBoat = sendData[4]; // Indica se é um barco random ou não

        game.savePlacedBoat(cell_id, gameID, numPlayer, boatNum, io, randomBoat);

    }) 




    // Quando um jogador faz reset dos barcos 
    socket.on('resetAllBoats', function (sendData) { 
        var gameID = sendData[0]; // Id do jogo
        var numPlayer = sendData[1]; // Número do jogador 
        game.resetAllBoats(gameID, numPlayer, io); // Apaga todos os barcos colocados deste utilizador neste jogo
    }) 



    // Quando um jogador dá um tiro
    socket.on('shot', function (sendData) {
        var cell_id = sendData[0]; // Posição em que foi feito o tiro
        var gameID = sendData[1]; // Id do jogo
        var numPlayer = sendData[2]; // Número do jogador
        game.shot(cell_id, gameID, numPlayer, io);
    })
    
    
    // Quando é pedido uma lista dos jogos
    socket.on('gamesList', function () {

        game.gamesList(socket); // Socket vai devolver a quem pediu a lista
        
    });




    // Quando é pedido a lista de jogos não finalizados de um utilizador
    socket.on('notConcludedGamesOfUser', function(userID) {

        game.notConcludedGamesOfUser(userID, socket); // Socket vai devolver a quem pediu a lista

    });



    // Quando é pedido a lista de rankings
    socket.on('rankingsList', function() {

        game.rankingsList(socket); // Socket vai devolver a quem pediu a lista

    });



    // Quando é escolhido para colocar os barcos automaticamente
    socket.on('automaticPlacingBoats', function () {
        
        socket.emit('automaticPlacingBoats', 'A1'); // Avisa o cliente

    });



     // Quando é escolhido para fazer reset a todos os barcos
     socket.on('resetBoats', function () {
        
        socket.emit('resetBoats', 'A1'); // Avisa o cliente

    });





     // Quando é escolhido para carregar um jogo existente
     socket.on('loadGame', function (sendData) {

        var gameID = sendData[0]; // ID do jogo
        var userID = sendData[1]; // ID do jogador
        
        socket.join('room' + gameID); // Entrar no jogo (para as mensagens serem enviadas para o grupo em específico)
        io.sockets.in('room' + gameID).emit("roomJoined", gameID);

        // Servidor avisa aos 2 jogadores qual é que joga no momento
        var whoStarts = Math.floor(Math.random() * 2); // Random (0 ou 1) - jogador que começa
        io.sockets.in('room' + gameID).emit("currentPlayer", whoStarts);
        
        game.loadGame(gameID, io, userID); // Socket vai devolver a quem pediu a lista

    });




    // Quando um jogador sai do jogo
    socket.on('leaveGame', function (sendData) {

        var playerID = sendData[0]; // Nº do jogador
        var gameID = sendData[1]; // Nº do jogo
        
        // Envia mensagem para o chat a indicar que o utilizador saiu
        io.sockets.in('room' + gameID).emit("receivedMessage", "O jogador " + io.sockets.adapter.rooms["room" + gameID].playersNames[playerID] + " saiu do jogo");
        
    });



     // Quando um jogador envia uma mensagem para o chat
     socket.on('sendMessage', function (sendData) {

        var message = sendData[0]; // Mensagem
        var gameID = sendData[1]; // Nº do jogo
        var playerID = sendData[2]; // Jogador

        var sendData = [io.sockets.adapter.rooms["room" + gameID].playersNames[playerID], message]; // Envia mensagem e o utilizador que escreveu
        
        // Envia mensagem para o chat a indicar que o utilizador saiu
        io.sockets.in('room' + gameID).emit("addMessage", sendData);
        
    });





    // Quando acaba de adicionar os 5 barcos
    socket.on('finishedAddingBoats', function (sendData) {

        var gameID = sendData[0]; // ID do jogo
        var playerNum = sendData[1]; // Num do jogador

        // Quando um jogador coloca os 5 barcos, envia a mensagem finishedAddingBoats para o servidor, juntamente com o id do jogo
        // A variável playersFinished (que conta o nº de jogadoress que terminaram de colocar barcos) é incrementada, desse mesmo jogo
        io.sockets.adapter.rooms["room" + gameID].playersFinished++;


        // Envia mensagem para o chat a indicar que está pronto (já colocou os barcos)
        io.sockets.in('room' + gameID).emit("receivedMessage", "O jogador " + io.sockets.adapter.rooms["room" + gameID].playersNames[playerNum] + " está pronto");



        // Se os 2 jogadores tiverem colocado os 5 barcos, é enviado mensagem para os dois (que estão no quarto), a indicar que podem começar a jogar
        if (io.sockets.adapter.rooms["room" + gameID].playersFinished == 2) {
            game.allBoatsPlaced(gameID, io); // Envia mensagem aos 2 jogadores do quarto a avisar para começarem a jogar
        }
    });




    // Esta função é chamada quando o utilizador roda o dado (para ver qual o primeiro a começar)
    socket.on('rolledDice', function (data) {

        var diceVal = data[0]; // Valor do dado
        var gameID = data[1]; // Nº do jogo
        var numPlayer = data[2]; // Nº do jogador
        var whoStarts = 0; // Jogador que começa o jogo

        if (numPlayer == 0) io.sockets.adapter.rooms["room" + gameID].player0 = diceVal; // Atualiza o valor do dado do jogador 0
        else if (numPlayer == 1) io.sockets.adapter.rooms["room" + gameID].player1 = diceVal; // Atualiza o valor do dado do jogador 1
        
        // O número de jogadores que rodaram o dado incrementa
        io.sockets.adapter.rooms["room" + gameID].playersFinishedRoll++; // Quando jogador roda o dado, incrementa o número

        // console.log("FINISHED ROLL" + playersFinishedRoll + " " + numPlayer + " " + diceVal);


        // Envia mensagem para o chat a indicar que está rodou o dado 
        io.sockets.in('room' + gameID).emit("receivedMessage", "O jogador " + io.sockets.adapter.rooms["room" + gameID].playersNames[numPlayer] + " rodou o dado e obteve " + diceVal);

        
        // Se os dois jogadores já rodaram o dado
        if (io.sockets.adapter.rooms["room" + gameID].playersFinishedRoll == 2) { // Caso os dois jogadores já tenham rodado o dado

            var player0dice = io.sockets.adapter.rooms["room" + gameID].player0; // Vai buscar o valor do dado do jogador 0 (naquele jogo)
            var player1dice = io.sockets.adapter.rooms["room" + gameID].player1; // Vai buscar o valor do dado do jogador 1 (naquele jogo)

            if (player0dice > player1dice) { // Caso o dado do jogador 0 tenha valor superior ao dado do jogador 1
                whoStarts = 0;
            } else if (player1dice > player0dice) { // Caso o dado do jogador 1 tenha valor superior ao dado do jogador 0
                whoStarts = 1;
            } else if (player1dice == player0dice) {
                whoStarts = Math.floor(Math.random() * 2); // Caso os dados sejam empatados, é random (0 ou 1)
            }

            game.tellWhoStarts(gameID, whoStarts, io);
        }
        

    });


    // Quando é pedido uma lista dos jogadores de um determinado jogo (no GameLobby)
    socket.on('requestUsers', function (gameID) { // Recebe o pedido dos jogadores do jogo
        game.getPlayersOfGame(gameID, socket, io);
    });


    // Quando é pedido para entrar dentro de um jogo
    socket.on('joinGame', function (sendData) {

        var gameID = sendData[0]; // ID do jogo
        var userID = sendData[1]; // ID do jogador

        // Entrar no jogo para enviar mensagens apenas para lá
        socket.join('room' + gameID); // Entrar no jogo (para as mensagens serem enviadas para o grupo em específico)
        
        if (!io.sockets.adapter.rooms["room" + gameID].players) {
            io.sockets.adapter.rooms["room" + gameID].players = []; // Inicializa o array dos jogadores do quarto com os seus ids
            io.sockets.adapter.rooms["room" + gameID].playersNames = []; // Inicializa o array dos jogadores do quarto com os seus nomes
        }

        io.sockets.adapter.rooms["room" + gameID].players.push(userID); // Guarda o id do utilizador nos jogadores do quarto
        io.sockets.adapter.rooms["room" + gameID].playersNames.push(socket.request.session.username); // Guarda o nome do utilizador nos jogadores do quarto

        game.joinGame(gameID, io, socket, userID, socket.request.session.username); // Entrar no jogo (adicionar na base de dados)

    });


    // Quando é informado que o jogo está cheio (tem 2 pessoas)
    socket.on('gameFull', function (gameID) {
        game.startGame(gameID, io); // O servidor manda começar o jogo para os 2 jogadores
    });


    // Quando é pedido para criar um jogo (quando recebe socket do frontend)
    socket.on("createGame", function (roomName) {
        let gameid = game.createNewGame(io, roomName); // Cria o jogo e devolve o id do jogo (para depois entrar no jogo - no join)
    });


    // Quando o jogador avisa que já colocou todos os barcos (e está pronto para jogar)
    /* socket.on("allBoatsPlaced", function (gameID) {
        game.startPlaying(gameID, io);
    }); */


    socket.on('disconnect', function(){
        userCount --;
        console.log("disconnected from sockket")
    })
    
});

io.on('disconnect', function(){
    userCount --;
    console.log("disconnected")
})

server.listen(process.env.PORT || 3000, function () {
    console.log("Battlechip server working.")
    
})