var User = require('./models/User.js');
var url = require('url');

function login(req,res){
    User.getAccount(req.body.email, req.body.password) // Vai buscar uma conta com esses dados
    .then(function(val){ // Devolve o resultado da função (boolean)
        if(!val){
             // Caso NÃO exista uma conta com esse email e password
            let path = url.format({ // Cria uma rota com variáveis associadas porque o GET do /register chama o index.ejs que precisa dessas 2 variáveis
                pathname:"/login",
                query: {
                   "route": 'login',
                   "msg":"Wrong credentials"
                 }
                });
            req.session.isLogged = false;
            res.redirect(path);
        }else {
            // Caso exista uma conta com esse email e password
            User.findOne({email: req.body.email},'username _id').exec()
            .then(function(user){
                req.session.username = user.username;
                req.session.userID = user._id;
                

                // Vai buscar id dessa conta e guarda numa variável de sessão
                req.session.isLogged = true; // Guarda variável indicando que tem sessão iniciada
                req.session.email = req.body.email; // Guarda email na variável de sessão
                console.log(req.session)
                res.redirect('/'); // Redireciona para o início
            })
        }

    })
}

function register(req,res){
    let exists;
    User.countDocuments({email: req.body.email}) // Devolve o nº de registos de User com este email (vindo do formulário)
        .then(function(count){  // Devolve o nº de utilizadores com esse email
            if(count > 0) exists = true; 
            else exists = false 
            if(exists){ // Se já existir algum utilizador com este email
                let path = url.format({ // Cria uma rota com variáveis associadas porque o GET do /register chama o index.ejs que precisa dessas 2 variáveis
                    pathname:"/register",
                    query: {
                       "route": 'register',
                       "msg":"User exists"
                     }
                    });
                res.redirect(path); // Redireciona para o caminho pedido
            }else{ // Se não existtr nenhum utilizador com este email
                var newUser = new User(); // Cria utilizador
                newUser.email = req.body.email; // Atribui email
                newUser.password = req.body.password; // Atribui password
                newUser.username = req.body.username; // Atribui username (vindo do formulário)
                newUser.save() // Guarda o utilizador na base de dados
                    .then(function(user){ // Se guardar com sucesso
                        req.session.username = user.username;
                        req.session.userID = user.id // Vai buscar id dessa conta e guarda numa variável de sessão
                        req.session.isLogged = true; // Guarda boolean indicando que está logado
                        req.session.email = user.email; // Guarda email numa variável de sessão
                        res.redirect("/") // Redireciona para a base
                    })
                    .catch(function(err){
                        console.log("An error occured saving the user "+err);
                    })
                }
        })
        .catch(function(err){
            console.log("Ocorreu um erro"+ err)
        })
}

module.exports= {
    login,
    register
}