const path = require("path");

module.exports = {
  outputDir: path.resolve(__dirname, "../battlechip_backend/views/vue"),
  assetsDir: "",
  productionSourceMap: false,
  indexPath: 'game.ejs',
  publicPath: 'vue/'
}
/* publicPath: 'vue/' needed for this to work in prod mode */