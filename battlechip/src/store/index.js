import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        // Estado inicial
        currentBoat: {},
        boatOrientation: 'up',
        placingBoat: true,
        diceMode: false,
        num_boats: 0,
        gameID: '',
        userID: '',
        num_player: 0,
        yourTurn: false,
        boatsList: [],
        ready: false,
        automaticPlacingMode: true, // Pode ser usado
        alreadyRolled: false,
        hit: new Audio("sfx/hit.wav"),
        miss: new Audio("sfx/miss.wav")
    },
    mutations: {
        resetState (state) {
            state.currentBoat = {};
            state.boatOrientation = 'up';
            state.placingBoat = true;
            state.diceMode = false;
            state.num_boats = 0;
            state.yourTurn = false;
            state.boatsList = [];
            state.ready = false;
            state.automaticPlacingMode = true;
            state.alreadyRolled = false;
            /* eslint-disable no-console */
            console.log(state);
        },

        setReady(state) { // Utilizador indica que está pronto, ao clicar no botão
            state.ready = true;
        },
        removeAutomaticMode (state) { // Remover o botão de colocar barcos automáticos
            state.automaticPlacingMode = false;
        },
        setBoat(state, boat){ // Permite selecionar um barco ao clicar nele
            state.currentBoat = boat;
        },
        placeBoat(state, bool){
            state.currentBoat.isPlaced = bool;
        },
        increaseNumBoats(state) { // Incrementa cada vez que é colocado um barco (para ver quando todos estão colocados)
            state.num_boats++;
        },
        resetNumBoats(state) { // Quando é feito reset, coloca o nº de barcos colocados na grid a 0
            state.num_boats = 0;
        },
        setYourTurn(state, bool) { // Altera o turno do jogador
            state.yourTurn = bool;
        },
        togglePlacingBoat(state) {
            // Cada vez que é chamado este método, o valor da variável placingBoat vai mudando
            if (state.placingBoat) {
                state.placingBoat = false;
            } else {
                state.placingBoat = true;
            }
        },
        disablePlacingBoat(state) { // Permite parar de colocar barcos (remover a grid dos barcos)
            state.placingBoat  = false;
        },
        diceMode(state, bool) { // Permite colocar e remover o botão para rodar o dado (para ver quem é o primeiro jogador)
            state.diceMode = bool;
        },
        setAlreadyRolled(state) {
            state.alreadyRolled = true;
        },
        toggleOrientation(state){
            // Permite rodar a orientação dos barcos (para colocar em posições diferente, nas célulaas)
            if (state.boatOrientation == 'up') { state.boatOrientation = 'right'; return }
            if (state.boatOrientation == 'right') { state.boatOrientation = 'down'; return }
            if (state.boatOrientation == 'down') { state.boatOrientation = 'left'; return }
            if (state.boatOrientation == 'left') { state.boatOrientation = 'up'; return }
        },
        setGameID(state,aGameID){ // Permite guardar o id do jogo atual
            state.gameID = aGameID
        },
        setUserID (state, userID) { // Permite guardar o id do utilizador atual
            state.userID = userID;
        },
        setPlayer(state, num_player) { // Permite atribuir um número ao jogador (0 ou 1)
            state.num_player = num_player;
        },
        setBoatsList(state, boatsList) {
            state.boatsList = boatsList;
        },
        playHit(state){
            state.hit.play()
            /* eslint-disable no-console */
            console.log("we are @"+process.env.BASE_URL)
        },
        playMiss(state){
            state.miss.play()
        }
    },
    actions: {
        "SOCKET_CONNECT"() {
        }
    }
})