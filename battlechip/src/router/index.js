import Vue from 'vue'
import Router from 'vue-router'
import Gamelist from '../components/GameList'
import GameLobby from '../components/GameLobby'
import GameBoards from '../components/GameBoards'
import Profile from '../components/Profile'
import Ranking from '../components/Ranking'
import Winner from '../components/Winner'


Vue.use(Router)

export default new Router({
    routes:[
        {
            path:'/gamelist',
            name: "GameList",
            component: Gamelist
        },
        {
            path:'/gamelobby/:gameID',
            name:"GameLobby",
            component: GameLobby,
            props: true
        },
        {
            path:'/game/:gameID',
            name:'GameBoards',
            component: GameBoards,
            props:true
        },
        {
            path:'/profile',
            name:'Profile',
            component: Profile,
            props: true
        },
        {
            path:'/ranking',
            name:'Ranking',
            component: Ranking,
            props: true
        },
        {
            path:'/winner',
            name:'Winner',
            component: Winner,
            props: true
        }
    ]
})
